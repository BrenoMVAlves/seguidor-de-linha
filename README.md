Projeto que conquistou o 2º lugar no Desafio Circuito Integrado na modalidade Seguidor de Linha (competição realizada pelo PET Elétrica UFCG).  
Trata-se de com carro controlado por um microcontrolador que deve seguir um caminho por uma trajetória curviliea desenhada no solo.


Projeto do Seguidor de Linha 

    Microcontrolar:     Arduino Uno
    Sensores:           3 Sensores de Infravermelho
    Motores:            2 motores na traseira
    Rodas:              2 Rodas traseiras e uma roda boba dianteira
    Chassis:            Estrutura montada pelos integrantes com peças improvisadas.
    Circuito Impresso:  Placa caseira utilizando fenolite. 
