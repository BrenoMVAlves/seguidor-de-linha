/* -------------- CARRO DO LEITE ---------------- /* 
 *  
 *  Authors:  Breno Machado
 *            Vinicius Freire
 *            Thiago José
 *  
 *  Ultima Alteração: 24/10/2017
 *    - 3 Sensores IR conectados
 *    - Motores Concentados 
 *    - Segue Linha, curvas de 90º e crossover. 
 */   
 
/*-- Declaração das Portas --*/
/* Conexões dos Motores */ 
const int Motor_Right_A = 10;         // B-A1
const int Motor_Right_B = 11;         // B-1B
const int Motor_Left_A = 5;           // A-1A
const int Motor_Left_B = 6;           // A-1B

/* Sensores IR  */ 
const int IR_Left_port = 0;   //Analógico
const int IR_Center_port = 1; //Analógico
const int IR_Right_port = 2;  //Analógico


/*-- Declaração das Variáveis --*/
/* Motores */ 

int PWM_Right = 0;
int PWM_Left = 0;

/* Sensores IR  */ 
int IR_Left = 0;
int IR_Right = 0;
int IR_Center = 0;

const int duty_cycle = 600;
const int IR_threshold = 300;
const int nstop = 0;
const int turnH = 800;
const int turn90 = 800;
char Status = '0';

void setup() 
{
  Serial.begin(9600);  
  pinMode(Motor_Right_A, OUTPUT);
  pinMode(Motor_Right_B, OUTPUT);
  pinMode(Motor_Left_A, OUTPUT);
  pinMode(Motor_Left_B, OUTPUT);
  
  pinMode(IR_Left_port, INPUT);
  pinMode(IR_Right_port, INPUT);
  pinMode(IR_Center_port, INPUT);          
}

void loop() 
{
  // Leitura dos Sensores
  IR_Left = analogRead(IR_Left_port);
  IR_Right = analogRead(IR_Right_port);
  IR_Center = analogRead(IR_Center_port);

  Serial.print("Esquerdo:   ");
  Serial.println(IR_Left);
  Serial.print("Direito:    ");
  Serial.println(IR_Right);
  Serial.print("Centro:    ");
  Serial.println(IR_Center);
  
  // Instanciação dos possíveis Casos
  if( LeuBranco(IR_Left) && LeuPreto(IR_Center) && LeuBranco(IR_Right)){                  //Go on 
    Segue_o_jogo();
  }
   else if(LeuBranco(IR_Left) && LeuBranco(IR_Center) && LeuBranco(IR_Right)){            //Tudo Branco  
    if(IR_Left > IR_Right)
      Turn('R');
    else if(IR_Left < IR_Right)
      Turn('L');       
 
  }
  
  else if( LeuBranco(IR_Left) && LeuBranco(IR_Center) && LeuPreto(IR_Right)){             //90º Direita
    Status = 'R';
    Turn_90('R');
  }
  else if( LeuPreto(IR_Left) && LeuBranco(IR_Center) && LeuBranco(IR_Right)){              //90º Esquerda
    Status = 'L';
    Turn_90('L');
  }
  else if( LeuPreto(IR_Left) && LeuBranco(IR_Center) && LeuPreto(IR_Right)){             // Durante a curva de 90º
    if(Status == 'R')
    {
      Turn_90('R');
      Status = '0';
    }
  else if(Status == 'L')
    {
      Turn_90('L');  
      Status = '0';   '1 
    }
  }
    else if(LeuBranco(IR_Left) && LeuPreto(IR_Center) && LeuBranco(IR_Right)){  //Suave Esquerda
    Turn('L');
  }
  else if(LeuBranco(IR_Left) && LeuPreto(IR_Center) && LeuPreto(IR_Right)){  //Suave Direita
    Turn('R');
  }
  else if( LeuPreto(IR_Left) && LeuPreto(IR_Center) && LeuPreto(IR_Right)){
    Segue_o_jogo();
    }
  
}// Fim Loop

bool LeuBranco(int leitura)
{
  return leitura < IR_threshold ? 1:0;
}
bool LeuPreto(int leitura)
{
  return leitura > IR_threshold ? 1:0;
}

void Segue_o_jogo()
{
  //Atribuiçao do duty cycle aos pwm's
  PWM_Right = map(duty_cycle,0 ,1023 ,0, 255);
  PWM_Left = map(duty_cycle,0 ,1023 ,0, 255);

  //Acionamento dos motores
  analogWrite(Motor_Right_A, PWM_Right);
  analogWrite(Motor_Right_B, LOW);
  analogWrite(Motor_Left_A, PWM_Left);
  analogWrite(Motor_Left_B, LOW);
}


void Parada()
{ 
  PWM_Right = map(nstop,0 ,1023 ,0, 255);
  PWM_Left = map(nstop,0 ,1023 ,0, 255);
  
  //Para os motores
  analogWrite(Motor_Right_A, PWM_Right);
  analogWrite(Motor_Right_B, LOW);
  analogWrite(Motor_Left_A, PWM_Left);
  analogWrite(Motor_Left_B, LOW);  
}


void Turn( char Direction )
{
  if(Direction == 'L')
  {
    //Atribuiçao do duty cycle aos pwm's
    PWM_Right = map(turnH,0 ,1023 ,0, 255);
    PWM_Left = map(turnH,0 ,1023 ,0, 255);

    //Acionamento dos motores
    analogWrite(Motor_Right_A, PWM_Right);
    analogWrite(Motor_Right_B, LOW);
    analogWrite(Motor_Left_A, LOW);
    analogWrite(Motor_Left_B, PWM_Left);

    
  }
  else if(Direction == 'R')
  {
    //Atribuiçao do duty cycle aos pwm's
    PWM_Right = map(turnH,0 ,1023 ,0, 255);
    PWM_Left = map(turnH,0 ,1023 ,0, 255);

    //Acionamento dos motores
    analogWrite(Motor_Right_A, LOW);
    analogWrite(Motor_Right_B, PWM_Right);
    analogWrite(Motor_Left_A, PWM_Left);
    analogWrite(Motor_Left_B, LOW); 
    
  }
}//Fim Turn


void Turn_90(char Direction){
  if(Direction == 'R'){                      //Curva 90º Direita
    PWM_Right = map(turn90,0 ,1023 ,0, 255);
    PWM_Left = map(turn90,0 ,1023 ,0, 255);

    //Acionamento dos motores
    analogWrite(Motor_Right_A, PWM_Right);
    analogWrite(Motor_Right_B, LOW);
    analogWrite(Motor_Left_A, LOW);
    analogWrite(Motor_Left_B, PWM_Left);    //Motor direito rode pra frente e esquerdo para trás
  }
  else if(Direction == 'L'){                     //Curva 90º Esquerda
    PWM_Right = map(turn90,0 ,1023 ,0, 255);
    PWM_Left = map(turn90,0 ,1023 ,0, 255);

    //Acionamento dos motores
    analogWrite(Motor_Right_A,LOW);
    analogWrite(Motor_Right_B,  PWM_Right);
    analogWrite(Motor_Left_A, PWM_Left);
    analogWrite(Motor_Left_B, LOW);
  }
  
}
